import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { AllProductsComponent } from './all-products/all-products.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
    path : "", redirectTo: "home" , pathMatch : "full"
  },
  {
    path : "home", component : HomeComponent
  },
  
  {
    path : "register" , component : RegisterComponent
  },
  {
    path : "login", component : LoginComponent
  },
  {
    path : "search" , component : SearchComponent
  },
  {
    path : "all-products", component : AllProductsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
