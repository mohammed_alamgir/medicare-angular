import { Component, OnInit } from '@angular/core';
import { UserServiceService} from '../user-service.service';
import {ProductServiceService} from '../product-service.service';
@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})

export class AllProductsComponent implements OnInit {

  products : any;
  constructor(private service : ProductServiceService) { }

  ngOnInit(){
    let response = this.service.getProducts();
    response.subscribe(data=> this.products=data);
  }

  public addProduct(title : string){
    let response = this.service.doSave(title);
    response.subscribe(data => this.products = data);
  }

}
