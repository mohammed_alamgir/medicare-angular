import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private http: HttpClient) { }

  public doSave(product)
  {
    return this.http.post("http://localhost:8080/save-product", product, {responseType : "text" as "json"});
  }
  
  public getProducts(){
    return this.http.get("http://localhost:8080/all-products");
  }

  public getProductByTitle(title){
    return this.http.get("http://localhost:8080/search-product/"+title);
  }
}
