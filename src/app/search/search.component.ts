import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../product-service.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  title : string;
  product : any;
  constructor(private service : ProductServiceService) { }

  ngOnInit(): void {
  }
  public findProductByTitle(){
    let response = this.service.getProductByTitle(this.title);
    response.subscribe(data => this.product = data)
  }

}
